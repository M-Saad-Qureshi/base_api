﻿namespace Base_API
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.txtBxOuput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBxURL = new System.Windows.Forms.TextBox();
            this.btnMailchimp = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(396, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtBxOuput
            // 
            this.txtBxOuput.Location = new System.Drawing.Point(12, 99);
            this.txtBxOuput.Multiline = true;
            this.txtBxOuput.Name = "txtBxOuput";
            this.txtBxOuput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBxOuput.Size = new System.Drawing.Size(452, 364);
            this.txtBxOuput.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "REST URL";
            // 
            // txtBxURL
            // 
            this.txtBxURL.Location = new System.Drawing.Point(79, 24);
            this.txtBxURL.Name = "txtBxURL";
            this.txtBxURL.Size = new System.Drawing.Size(311, 20);
            this.txtBxURL.TabIndex = 3;
            this.txtBxURL.Text = "https://api.getbase.com/v2/contacts";
            // 
            // btnMailchimp
            // 
            this.btnMailchimp.Location = new System.Drawing.Point(194, 70);
            this.btnMailchimp.Name = "btnMailchimp";
            this.btnMailchimp.Size = new System.Drawing.Size(75, 23);
            this.btnMailchimp.TabIndex = 4;
            this.btnMailchimp.Text = "Check";
            this.btnMailchimp.UseVisualStyleBackColor = true;
            this.btnMailchimp.Visible = false;
            this.btnMailchimp.Click += new System.EventHandler(this.btnMailchimp_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(101, 70);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(13, 13);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Total Uploaded:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 475);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnMailchimp);
            this.Controls.Add(this.txtBxURL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBxOuput);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtBxOuput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBxURL;
        private System.Windows.Forms.Button btnMailchimp;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label2;
    }
}

