﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;


namespace Base_API
{
    public partial class Form1 : Form
    {
        String getContactAPIURL = "";

        /// /// Web Request Related Variables
        /// 
        String USER_AGENT_MOZILLA = "Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0";

        // AccessToken of my Base account      
        
         String BaseAccessToken = "a92ead83dd0432eb68ae1b1dcced346a3e10bc9199606f7d56c62278d242e761";


        /// ////////////// Mailchimp related variables
        String mailchimpauthURL = "https://us12.api.mailchimp.com/3.0/";

        // Link to post JSON for adding subscriber to my list named as "test" in mailchimp
        // I have added 2 things of my account of Mailchimp in below URL
        // us12 = is my api server
        // b31c7f923b = Id of "test" list
        String addSubsURL = "https://us12.api.mailchimp.com/3.0/lists/b31c7f923b/members/"; // Your own api server and list id

        String mailchimpKey = "a71d184fcbf38a982964d64d85a200be-us12";  // Your own Mailchimp API key
        String mcUsername = "ssgcommando"; // Your own Mailchimp username
        CookieContainer cookieJar = new CookieContainer();
        HttpWebResponse webResponse;



        public Form1()
        {
            InitializeComponent();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            String baseAuthURL = "https://api.getbase.com/v2/users/self";

            Boolean allValid = GetUIInputs();

            if (allValid == false)
            {
                MessageBox.Show("Please fill URL", "Problem", MessageBoxButtons.OK);
                return;
            }

            // Authentication from Base
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(baseAuthURL);
            webReq.Accept = "application/json";
            webReq.Headers.Add("Authorization", "Bearer " + BaseAccessToken);
            String response = GetResponse(webReq, "", false, "GET", "", true);

            // Getting all contacts from Base
            webReq = (HttpWebRequest)WebRequest.Create(getContactAPIURL);
            webReq.Accept = "application/json";
            webReq.Headers.Add("Authorization", "Bearer " + BaseAccessToken);
            response = GetResponse(webReq, "", false, "GET", "", true);

            if (String.IsNullOrEmpty(response) == false)
            {
                txtBxOuput.Text = response;

                try
                {
                    Class1.RootObject root = JsonConvert.DeserializeObject<Class1.RootObject>(response);

                    // Authentication from Mailchimp
                    HttpWebRequest _mcWebReq = getWebRequestMailChimp(mailchimpauthURL);
                    String _response = GetResponse(_mcWebReq, "", false, "GET", "", true);

                    //for (Int32 pageNumber = 1; pageNumber <= root.meta.count; pageNumber++)
                    //  for (Int32 pageNumber = 1; pageNumber <= 2; pageNumber++)

                    bool firstTime = true;
                    Int16 totalUploded = 1;
                    while (String.IsNullOrEmpty(root.meta.links.next_page) == false)
                    {
                        if (firstTime == false)
                        {
                            // Getting all contacts from Base
                            webReq = (HttpWebRequest)WebRequest.Create(root.meta.links.next_page);
                            webReq.Accept = "application/json";
                            webReq.Headers.Add("Authorization", "Bearer " + BaseAccessToken);
                            response = GetResponse(webReq, "", false, "GET", "", true);

                            root = JsonConvert.DeserializeObject<Class1.RootObject>(response);


                        }

                        firstTime = false;

                        for (Int32 index = 0; index < root.items.Count; index++)
                        {
                            if (root.items[index].data.email != null)
                            {
                                String email = root.items[index].data.email;
                                String fName = root.items[index].data.first_name;
                                String lName = root.items[index].data.last_name;
                                String status = "subscribed";

                                // Posting each contacts to Mailchimp

                                _mcWebReq = getWebRequestMailChimp(addSubsURL);
                                String json = getJSONBody(email, fName, lName, status);
                                _response = GetResponse(_mcWebReq, "", false, "POST", json, true);

                                if (_response.IndexOf(status) > -1)
                                {
                                    lblStatus.Text = totalUploded++.ToString();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    System.Console.WriteLine(ex.StackTrace);
                }

            }
        }

        private string GetResponse(HttpWebRequest webReq, string Referer, Boolean UseProxy, string Method, string Data, bool allowAutoRedirect)
        {
            string htmlResponse = string.Empty;

            try
            {
                webReq.CookieContainer = cookieJar;

                webReq.ReadWriteTimeout = 15000; // it is 15 seconds

                if (String.IsNullOrEmpty(webReq.Accept) == true)
                {
                    webReq.Accept = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
                }

                webReq.AutomaticDecompression = DecompressionMethods.Deflate;

                webReq.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us");

                webReq.KeepAlive = true;

                //  webReq.Headers.Add("Cache-Control", "no-cache");

                // webReq.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

                webReq.ServicePoint.Expect100Continue = false;

                if (UseProxy)
                {
                    //  WebProxy myproxy = new WebProxy(ip, port);
                    //  myproxy.BypassProxyOnLocal = false;
                    //  webReq.Proxy = myproxy;
                }

                webReq.ProtocolVersion = HttpVersion.Version11;

                webReq.AllowAutoRedirect = allowAutoRedirect;

                if (!string.IsNullOrEmpty(Referer))
                {
                    webReq.Referer = Referer;
                }

                webReq.UserAgent = USER_AGENT_MOZILLA;

                if (Method.ToLower() == "post")
                {
                    if (string.IsNullOrEmpty(Data) == false)
                    {
                        webReq.ContentLength = ASCIIEncoding.ASCII.GetBytes(Data).Length;

                        webReq.Method = "POST";

                        var data = ASCIIEncoding.ASCII.GetBytes(Data);

                        using (var stream = webReq.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }
                    }
                }

                webResponse = (HttpWebResponse)webReq.GetResponse();

                //  Uri uri = new Uri(URL);

                // if (cookieJar.GetCookieHeader(uri) != null)
                // {
                //     System.Console.WriteLine(cookieJar.GetCookieHeader(uri));
                // }

                int cookieCount = cookieJar.Count;

                Stream s = webResponse.GetResponseStream();
                using (StreamReader sr = new StreamReader(s))
                {
                    s.Flush();
                    htmlResponse = sr.ReadToEnd();

                }
            }


            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        htmlResponse = (sr.ReadToEnd());
                    }
                }

                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        htmlResponse = (sr.ReadToEnd());
                    }
                }
            }

            return htmlResponse;
        }

        private string getJSONBody(String email, String firstName, String lastName, String status)
        {
            String json = "";
            json = @"{" +
    @"""email_address"": """ + email + @"""," +
    @"""status"": """ + status + @"""," +
    @"""merge_fields"": {" +
                @"""FNAME"": """ + firstName + @"""," +
        @"""LNAME"": """ + lastName + @"""" +
   "}" +
        "}";

            return json;
        }

        /// <summary>
        /// It collects UI inputs
        /// </summary>
        /// <returns>All Inputs are valid or not</returns>
        private Boolean GetUIInputs()
        {
            Boolean allValid = false;
            try
            {
                if (String.IsNullOrEmpty(txtBxURL.Text) == false)
                {
                    getContactAPIURL = txtBxURL.Text;
                    allValid = true;
                }

                else
                {
                    allValid = false;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            return allValid;
        }

        private void btnMailchimp_Click(object sender, EventArgs e)
        {


        }

        private HttpWebRequest getWebRequestMailChimp(String url)
        {
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(url);
            webReq.Accept = "application/json";
            webReq.Headers.Add("Authorization", mcUsername + " " + mailchimpKey);

            return webReq;
        }
    }
}
