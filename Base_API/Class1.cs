﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base_API
{
    class Class1
    {
        public class Address
        {
            public string city { get; set; }
            public string line1 { get; set; }
            public string postal_code { get; set; }
            public string state { get; set; }
            public string country { get; set; }
        }

        public class CustomFields
        {
            public string paymentmethod { get; set; }
            public string paymentterms { get; set; }
            public string paymentmethod1 { get; set; }
            public string ContactCategory { get; set; }
        }

        public class Data
        {
            public int creator_id { get; set; }
            public int? contact_id { get; set; }
            public string created_at { get; set; }
            public string updated_at { get; set; }
            public object title { get; set; }
            public string name { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public object description { get; set; }
            public object industry { get; set; }
            public object website { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string mobile { get; set; }
            public object fax { get; set; }
            public object twitter { get; set; }
            public object facebook { get; set; }
            public object linkedin { get; set; }
            public object skype { get; set; }
            public int owner_id { get; set; }
            public bool is_organization { get; set; }
            public Address address { get; set; }
            public List<object> tags { get; set; }
            public CustomFields custom_fields { get; set; }
            public string customer_status { get; set; }
            public string prospect_status { get; set; }
        }

        public class Meta
        {
            public string type { get; set; }
        }

        public class Item
        {
            public Data data { get; set; }
            public Meta meta { get; set; }
        }

        public class Links
        {
            public string self { get; set; }
            public string next_page { get; set; }
        }

        public class Meta2
        {
            public string type { get; set; }
            public int count { get; set; }
            public Links links { get; set; }
        }

        public class RootObject
        {
            public List<Item> items { get; set; }
            public Meta2 meta { get; set; }
        }
    }
}
